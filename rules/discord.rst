##########################
Discord Rules & Guidelines
##########################

.. note:: All members are required by the community and the management to read through this agreement and the rules when joining our discord server. You are responsible for checking this rules list for any updates (**Last Update:** 2019-08-11 4:33 PM GMT). Rule may be updated at any time, it is your job to keep updated with them when joining the server it is implied that you accept these terms as well as you following them. 
	
	**By entering any of our discord server you agree to the following set of rules along with all rules for that individual server or site. You also agree, upon entering any community server​s, to accept any punishments for breaking said rules. You agree that you are responsible for the actions committed on your account during your visit.**

Advertisement & Poaching
========================

**You are strictly forbidden post any material or advertisements on our discord server, this discord server is not for advertising your own community nor will it ever be. This rule is NOT limited to following:**
	a) In-game advertisements such as business, events and other types of materials. These advertisements are limited to #lifeinvader, #car-for-sale, #weazel-news
	b) Admins approval of posting said advertisement or material

**You are strictly forbidden to poach any members in any way possible, including private messages. No exceptions.**

Bluetooth Channels
==================

1) **Before joining an occupied bluetooth channel please ask for permission from someone in the channel.**

2) **Bluetooth channels are to be kept in character ONLY, if anyone is found breaking character they will be removed from the bluetooth call and issued punishments.**

3) **If people are using voice channels for in character bluetooth chats, please do not interfere.**

Channel flooding/spam & Arguments
=================================

1) **You are strictly forbidden to spam or flood both voice and text channels in any way possible, this includes text channels as well as voice channels.**
	a) When you are posting content in any text channels make sure it's relevant to said channel, for example, posting in-game advertisements in general chat is wrong it should go under #lifeinvader, another example would be posting clips in the #sunrise-clips channel for opinions, etc. Ensure that the content your posting belongs to the channel you are trying to post it in. If not, do NOT post it. If you are unsure just ask it saves us all time.

2) **Keep arguments to a minimum and in the #dispute channel, it is strictly forbidden for arguments to leave said channel, for example, private messages.**
	a) Remember to keep it as an argument, it's strictly forbidden to attack someone.

Contacting staff
================

1) **We all know that it's super important to be able to reach staff members quickly but in some cases, it's not. You need to evaluate whether or not it's an emergency or not.**
	a) Reporting a player for breaking a rule can be done throughout our discord server, we have logs that cover far back enough to be able to do a full report a year after it was made so don't freak out if you haven't heard anything back within a few minutes or a few days. It all depends on how backed up admins are and other factors. They'll get to it as soon as possible.
	b) If you evaluate it as an emergency or you simply need an admins input please join the admin waiting channel and one of our admins will move you in as soon as possible.
	c) Channels available for support #in-game-support, #report-a-player, #discord-support

2) **Contacting staff directly through DMs and what not should be kept off-limit unless you are unable to reach any other admins. Keep in mind that this is ONLY for emergencies, if it's not an emergency please refrain from contacting admins directly through DMs.**

Harassment
==========

1) **You are under no circumstances allowed to harras a member of this community, not on the community or outside of the community. This includes social media.**

Lounges & Private Lounges
=========================

1) **Before joining an occupied private channel please ask for permission from someone in the channel.**

2) **Lounges & private lounges may NOT be used for in character purpose and anything said in any of those rooms must never be used for in character gain.**

Off-topic Material
==================

1) **Keep anything that is not server/character-related within the #off-topic-chat, we don't want to limit our discord server to only community-related materials but also have a casual chat as well.**

Role Abuse & Abusive Content
============================

1) **You are strictly forbidden to abuse any roles given to you, for example:**
	a) Leaking materials as a staff member from any staff related channels.
	b) Leaking materials from a whitelisted job.
	c) And much more...

2) **You are strictly forbidden to post anything offensive or sexual content, there are no exceptions!**

3) **This community does NOT tolerate racism or racial slurs.**