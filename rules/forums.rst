#############
Forum Rules
#############


1. Netiquette
=============
Violations of these Rules will result in one (1) warning point each.

1.1 Thread title
================
Your thread title should be legible, informative, and related to the content of your thread.

1.2 Choice of subforum
======================
Post your thread in the relevant subforum. Do not post several instances of the same thread in different subforums. Ask yourself "if I were looking for this thread, is this the first place I would try to find it?" If you accidentally post in the wrong subforum, ask moderators to move the thread. Do not open a duplicate.

1.3 Pointless & off-topic
=========================
Discuss the topic as defined in the original post. "TL;DR", "first!" and similarly useless posts fall under this rule, too. If for any reason you find yourself engaged in an off-topic discussion, please move to a more pertinent thread or continue via private messages.

1.4 No ads
==========
The forums is not an ad space/fundraiser. Any content (posts, signatures, profile details, etc.) published in order to sell a product/service or obtain personal gain will be removed. Multiple infractions in a short period will be considered spamming and can be subject to a temporary or permanent ban. Donations to users bearing the title "Recognized Developer" are exempt from this Rule. If you want to run a giveaway you can do although ensure you've read and understood the giveaway guidelines which can be found in the giveaway section of the forums. If you are still unsure or have questions related to the giveaway please contact a moderator. In general, if it benefits the community, we don't mind - if it only benefits you, we do.

2. Code of Conduct
==================
Violations of these rules are worth two (2) warning points each.

Members should post in a way that is respectful of other users. The following actions are prohibited from the Forums:

* Using inappropriate language, including sexually explicit, violent, or offensive language.
* Showing a lack of respect and tolerance towards any forums members.
* Attacking personally forum members based on religious, racial, political beliefs or any offensive grounds that are not related to the topic of this forum.
* “Doxxing” or threatening to reveal personal details of other users. You are advised not to reveal personal information about yourself in the forum.
* Posting malware/phishing websites and other content that is strictly harmful.
* Posting any intellectual property of a third party for which the member does not have express written permission to use, including, but not limited to trademarks, copyrights, trade secrets, and personality right. Please see the Sunrise Roleplay Copyright Policy if you have a takedown request.

Members who fail to respect the aforementioned code of conduct will be sanctioned/banned from the Sunrise Roleplay Forums and their posted content will be removed.

3. To avoid duplicating content, search similar threads before posting or creating a new discussion
===================================================================================================
New joiners and normal users are strongly advised to use search function before starting a new discussion. Other members may have already shared about the specific subject or addressed the question you are about to ask. If you post a thread that is very similar to already posted ones, it may get merged or closed by the moderation team.

4. Languages
============
Threads should be clear and include appropriate and descriptive subjects in order to provide more understanding to fellow members. This community is a global community, meaning any content must be in English only, if you wish to speak in your own language please refer the private message system.

5. Constructive Feedback
========================
At Sunrise Roleplay, we welcome your feedback. Sometimes, your feedback may be negative, and that’s okay. We want to hear your honest thoughts on our products and our brand as a whole. However, criticism should be legitimate, constructive and suggest solutions. Posts that exist solely to attack Sunrise Roleplay without basis will be removed. Action may be taken against users who continuously break this rule.

6. One Account Per User
=======================
On the forums, everyone’s voice should be equal. In that spirit, users are allowed ONE (1) account only. Users found to be using multiple accounts may have all accounts banned and/or removed. This includes accounts created to circumvent bans.

7. Editing / Closing / Deleting Threads
=======================================
The moderation and Admin team reserves the right to edit, merge, close, or delete any thread at any time. You will receive a notification explaining the reason why a thread has been modified or closed. If you are unhappy with the decision made by moderators, firstly reach out to the moderator responsible for the explanation. If that discussion is not fruitful, refer to an administrator. Admin decisions are final.

8. Warnings
===========
Warning points are given for certain infractions detailed above. The point of a warning is to alert you to the fact that you broke the forum rules. If you accumulate 5 warning points, you will be banned for 1 day. At 10 points, you will be banned for a week. At 20, it’s a month and at 30, you will be banned permanently. Warning points expire two months after they have been given.